#* Imports
import requests
import getpass
import pprint
import pycook.elisp as el
import pycook.insta as st

#* Constants
port = "5000"
site = f"http://localhost:{port}"
db_name = "banko"

#* Recipes
def create_database(recipe):
    if db_name in st.scb("sudo -u postgres psql -c '\\l'"):
        print(f"database {db_name}: OK")
    else:
        user = getpass.getuser()
        st.bash([
            f"sudo -u postgres psql -c 'create database {db_name};'",
            f"sudo -u postgres psql -d {db_name} -c 'GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO {user}'"
        ])

def setup_postgres_permissions(recipe):
    user = getpass.getuser()
    return f"sudo -u postgres psql -c 'ALTER USER {user} WITH CREATEDB;'"

def create_account(recipe, name="Oleh Krehel"):
    data = {"name": name}
    r = requests.post(site + "/account", json=data)
    assert r.status_code == 200
    print(r.json())

def get_account(recipe, user_id="1"):
    r = requests.get(f"{site}/account/{user_id}")
    assert r.status_code == 200
    print(r.json())

def deposit(recipe, user_id="1"):
    data = {"amount": 100}
    r = requests.post(f"{site}/account/{user_id}/deposit", json=data)
    assert r.status_code == 200
    print(r.json())

def transfer(recipe):
    data = {"amount": 10, "account-number": 2}
    r = requests.post(f"{site}/account/1/send", json=data)
    assert r.status_code == 200
    print(r.json())

def audit(recipe):
    r = requests.get(f"{site}/account/1/audit")
    assert r.status_code == 200
    pprint.pprint(r.json())

def test(recipe):
    return "lein test banko.core-test"

def integration_test(recipe):
    return "PORT=6000 DBNAME=banko_test lein test banko.integration-test"

def concurrent_get(recipe):
    return f"ab -n 1000 -c 20 {site}/account/1"

def concurrent_deposit(recipe):
    return f"ab -p test/deposit.json -T application/json -n 1000 -c 20 {site}/account/1/deposit"

def concurrent_withdraw(recipe):
    return f"ab -p test/withdraw.json -T application/json -n 1000 -c 20 {site}/account/1/withdraw"

def concurrent_transfer(recipe):
    return f"ab -p test/transfer.json -T application/json -n 1000 -c 20 {site}/account/1/send"

def concurrent(recipe):
    return [
        "cook get_account 1",
        "cook get_account 2",
        "cook concurrent_deposit",
        "cook concurrent_transfer",
        "cook get_account 1",
        "cook get_account 2"]
