(ns banko.integration-test
  (:require [clojure.test :refer [deftest]]
            [banko.core :as banko]
            [banko.sql :as sql]
            [restpect.core :refer [created ok not-found]]
            [restpect.json :refer [POST GET]]))

(banko/main)
(sql/clear-test-db)

(defn url [& args]
  (apply str (conj args (str "http://localhost:" (or (System/getenv "PORT") "5000")))))

(deftest rest-api-test
  (let [user-name "Mr. Black"
        id (:account-number (:body
                             (ok
                               (POST (url "/account") {:name user-name})
                               {:account-number integer?
                                :balance 0
                                :name user-name})))]
    (ok
      (GET (url "/account/" id) {})
      {:account-number id
       :name user-name
       :balance 0})
    (ok
      (POST (url "/account/" id "/deposit") {:amount 100})
      {:account-number id
       :name user-name
       :balance 100})))
