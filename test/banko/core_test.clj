(ns banko.core-test
  (:require [clojure.test :refer [deftest is use-fixtures]]
            [banko.data :as data]))

(deftest create-account
  (is (data/create-account "Mr. Black")
      {:name "Mr. Black" :balance 0}))

(deftest deposit-negative
  (is (thrown? IllegalArgumentException (data/deposit 1 -100)))
  (is (thrown? IllegalArgumentException (data/deposit 1 0))))

(deftest withdraw-negative
  (is (thrown? IllegalArgumentException (data/withdraw 1 -1))))

(deftest withdraw-overdraft
  (is (thrown? IllegalArgumentException (data/withdraw 1 1000))))

(deftest deposit-and-withdraw
  (let [old-balance (:balance @(data/get-account 3))]
    (is (= (:balance (do
                       (data/deposit 3 100)
                       (data/withdraw 3 100)))
           old-balance))))

(deftest transfer-overdraft
  (is (thrown? IllegalArgumentException (data/transfer 1 2 10000)))
  (is (thrown? IllegalArgumentException (data/transfer 1 1 10))))

(deftest audit
  (is (= (data/audit 1)
         '({"sequence" 3
            "description" "withdraw"
            "debit" 20}
           {"sequence" 2
            "description" "receive from #2"
            "credit" 10}
           {"sequence" 1,
            "description" "send to #3"
            "debit" 5}
           {"sequence" 0,
            "description" "deposit"
            "credit" 100}))))

(defn data-fixture [test-run]
  (reset! data/accounts [])
  (data/create-account "Mr. Black")
  (data/create-account "Mr. White")
  (data/create-account "Mr. Red")
  (data/deposit 1 100)
  (data/transfer 1 3 5)
  (data/deposit 2 1000)
  (data/transfer 2 1 10)
  (data/withdraw 1 20)
  (test-run))

(use-fixtures :each data-fixture)
