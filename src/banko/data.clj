(ns banko.data)

(def accounts (atom []))

(def queue (ref []))

(defn enqueue [item]
  (alter queue conj item))

(defn create-account [name]
  (let [id (inc (count @accounts))
        acc {:name name :balance 0 :logs ()
             :account-number id}]
    (dosync
      (swap! accounts conj (ref acc))
      (enqueue [:create-account id name]))
    acc))

(defn get-account [id]
  (@accounts (dec id)))

(defn -next-seq [acc]
  (let [old (ffirst (:logs @acc))]
    (if (nil? old)
      0
      (inc old))))

(defn deposit [id amount]
  (if (<= amount 0)
    (throw (IllegalArgumentException. "Non-positive amount")))
  (dosync
    (let [acc (get-account id)
          new-log [(-next-seq acc) :deposit amount nil]]
      (alter acc update :balance + amount)
      (alter acc update :logs conj new-log)
      (enqueue [:update-balance id (:balance @acc)])
      (enqueue [:add-log id new-log])
      @acc)))

(defn withdraw [id amount]
  (if (<= amount 0)
    (throw (IllegalArgumentException. "Non-positive amount"))
    (dosync
      (let [acc (get-account id)
            balance (:balance @acc)
            new-log [(-next-seq acc) :withdraw amount nil]]
        (if (> amount balance)
          (throw (IllegalArgumentException. "No overdrafts")))
        (alter acc update :balance - amount)
        (alter acc update :logs conj new-log)
        (enqueue [:update-balance id (- balance amount)])
        (enqueue [:add-log id new-log])
        @acc))))

(defn transfer [id1 id2 amount]
  (if (<= amount 0)
    (throw (IllegalArgumentException. "Non-positive amount")))
  (if (= id1 id2)
    (throw (IllegalArgumentException. "Cannot transfer to the same account")))
  (dosync
    (let [a1 (get-account id1)
          a2 (get-account id2)
          balance1 (:balance @a1)
          balance2 (:balance @a2)
          new-log-1 [(-next-seq a1) :send amount id2]
          new-log-2 [(-next-seq a2) :receive amount id1]]
      (if (> amount balance1)
        (throw (IllegalArgumentException. "No overdrafts")))
      (alter a1 update :balance - amount)
      (alter a1 update :logs conj new-log-1)
      (enqueue [:update-balance id1 (- balance1 amount)])
      (enqueue [:add-log id1 new-log-1])

      (alter a2 update :balance + amount)
      (alter a2 update :logs conj new-log-2)
      (enqueue [:update-balance id2 (+ balance2 amount)])
      (enqueue [:add-log id2 new-log-2])
      @a1)))

(defn audit [id]
  (map
    (fn [x]
      (let [[s action amount from] x]
        (merge
          {"sequence" s}
          (case action
            :send {"description" (str "send to #" from)
                   "debit" amount}
            :receive {"description" (str "receive from #" from)
                      "credit" amount}
            :withdraw {"description" "withdraw"
                       "debit" amount}
            :deposit {"description" "deposit"
                      "credit" amount}))))
    (:logs @(get-account id))))
