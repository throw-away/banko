(ns banko.sql
  (:require
   [clojure.java.jdbc :as sql]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [me.raynes.fs :as fs]
   [banko.data :as data]))

(defn read-env []
  (let [f (fs/expand-home "~/.config/banko/env.edn")]
    (if (fs/exists? f)
      (edn/read
        (java.io.PushbackReader.
          (io/reader f)))
      {})))

(def db (merge {:dbtype "postgresql"
                :dbname (or (System/getenv "DBNAME") "banko")
                :host "localhost"}
               (read-env)))

(defn migrated? [table-name]
  (->
    (sql/query
      db
      [(str
         "select count(*) from information_schema.tables where table_name='"
         table-name
         "' ;")])
    (first)
    (:count)
    (pos?)))

(defn create-table [db table]
  (sql/db-do-commands
    db
    (case (keyword table)
      :accounts (sql/create-table-ddl
                  table
                  [[:id :serial :primary :key]
                   [:name "varchar(32)"]
                   [:balance :bigint :default 0]])
      :logs (sql/create-table-ddl
              table
              [[:id :integer]
               [:seq :integer]
               [:action :integer]
               [:target :integer]
               [:amount :bigint]
               ["PRIMARY KEY" "(id, seq)"]]))))

(defn migrate []
  (when-not (migrated? "accounts")
    (create-table db "accounts"))
  (when-not (migrated? "logs")
    (create-table db "logs")))

(defn clear-test-db []
  (sql/db-do-commands (assoc db :dbname "postgres") false
                      ["drop database if exists banko_test"
                       "create database banko_test"])
  (migrate))

(def action-to-number {:send 0
                       :receive 1
                       :withdraw 2
                       :deposit 3})

(def number-to-action (clojure.set/map-invert action-to-number))

(defn mk-log [log]
  (map (fn [[_ s a t amount]]
         [s (number-to-action a) amount t])
       (reverse log)))

(defn init-from-db []
  (migrate)
  (let [accs (sort-by first (rest (sql/query db "select * from accounts" {:as-arrays? true})))
        logs (group-by first (rest (sql/query db "select * from logs" {:as-arrays? true})))]
    (vec
      (map (fn [[id name balance]]
             (ref
               {:name name
                :balance balance
                :account-number id
                :logs (mk-log (logs id))}))
           accs))))

(defn -new-log-row [id data]
  (let [[s action amount target] data]
    {:id id
     :seq s
     :action (action-to-number action)
     :target target
     :amount amount}))

(defn save-to-db []
  (sql/delete! db "accounts" [])
  (sql/delete! db "logs" [])
  (doseq [account @data/accounts]
    (sql/insert! db "accounts" {:id (:account-number @account)
                                :name (:name @account)
                                :balance (:balance @account)})
    (doseq [log (:logs @account)]
      (sql/insert! db "logs" (-new-log-row (:account-number @account) log)))))

(defn sync-to-db
  "Take all the items from the SQL queue and sync them to DB."
  []
  (let [actions-ref (ref [])]
    (dosync
      (ref-set actions-ref @data/queue)
      (ref-set data/queue []))
    (let [latest-updates
          (loop [actions @actions-ref
                 updates {}]
            (if-let [[[action & args] & tail] actions]
              (case action
                :create-account
                (let [[id name] args]
                  (sql/insert! db "accounts" {:id id :name name})
                  (recur tail updates))
                :update-balance
                (let [[id balance] args]
                  (recur tail (assoc updates id balance)))
                :add-log
                (let [[id x] args]
                  (sql/insert! db "logs" (-new-log-row id x))
                  (recur tail updates)))
              updates))]
      (doseq [[id balance] latest-updates]
        (sql/update! db "accounts" {:balance balance} ["id=?" id])))))

(defn db-syncer
  "Try to sync to DB every second.

  In case we're already syncing an old chunk, sleep.
  Otherwise, pull all items from the SQL queue into a new chunk and start syncing."
  []
  (loop [f (future (sync-to-db))]
    (Thread/sleep 1000)
    (if (realized? f)
      (recur (future (sync-to-db)))
      (recur f))))
