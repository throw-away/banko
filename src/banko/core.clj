(ns banko.core
  (:require [org.httpkit.server :as httpkit]
            [compojure.core :refer [defroutes GET POST]]
            [compojure.route :as route]
            [ring.middleware.json :as json_middleware]
            [ring.util.http-response :as response]
            [clojure.string :as str]
            [clojure.data.json :as json]
            [banko.sql :as sql]
            [banko.data :as data])
  (:gen-class))

(defmacro json-response [body]
  `(try
     (response/header
       (response/ok (json/write-str ~body))
       "Content-Type" "text/json")
     (catch IllegalArgumentException e# (response/bad-request (str e#)))
     (catch IndexOutOfBoundsException e# (response/bad-request "Account does not exist"))
     (catch Exception e# (response/bad-request "Unexpected exception"))))

(defn format-account [acc]
  (-> acc
      (dissoc :logs)))

(defn create-account [req]
  (let [body (:body req)
        user-name (body "name")]
    (if (string? user-name)
      (let [acc (data/create-account user-name)
            r (format-account acc)]
        (json-response r))
      (response/bad-request "No name supplied"))))

(defn get-account [req]
  (let [id (Integer/parseInt (:id (:params req)))]
    (if (<= id (count @data/accounts))
      (let [acc @(data/get-account id)
            r (format-account acc)]
        (json-response r))
      (response/bad-request "Account does not exist"))))

(defn deposit [req]
  (json-response
    (let [amount ((:body req) "amount")
          id (Integer/parseInt (:id (:params req)))
          acc (data/deposit id amount)]
      (format-account acc))))

(defn withdraw [req]
  (json-response
    (let [amount ((:body req) "amount")
          id (Integer/parseInt (:id (:params req)))
          acc (data/withdraw id amount)]
      (format-account acc))))

(defn transfer [req]
  (json-response
    (let [amount ((:body req) "amount")
          id1 (Integer/parseInt (:id (:params req)))
          id2 ((:body req) "account-number")
          acc (data/transfer id1 id2 amount)]
      (format-account acc))))

(defn audit [req]
  (json-response
    (let [id (Integer/parseInt (:id (:params req)))]
      (data/audit id))))

(defroutes app-routes
  (POST "/account" r (create-account r))
  (GET "/account/:id" r (get-account r))
  (POST "/account/:id/deposit" r (deposit r))
  (POST "/account/:id/withdraw" r (withdraw r))
  (POST "/account/:id/send" r (transfer r))
  (GET "/account/:id/audit" r (audit r))
  (route/resources "/")
  (route/not-found "Error, page not found!"))

(defonce server (atom nil))

(defn -restart-server []
  (when @server
    ;; wait 100ms for existing requests to terminate
    (@server :timeout 100)
    (reset! server nil))
  (let [port (Integer/parseInt (or (System/getenv "PORT") "5000"))]
    (reset! server
            (httpkit/run-server
              (json_middleware/wrap-json-body app-routes)
              {:port port}))
    (println (str "Running webserver at http:/127.0.0.1:" port "/"))))

(def sql-sync (Thread. sql/db-syncer))

(defn main
  "Main entry point"
  [& args]
  (reset! data/accounts (sql/init-from-db))
  (-restart-server)
  (. sql-sync start))
